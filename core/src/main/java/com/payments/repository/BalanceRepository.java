package com.payments.repository;

import com.payments.bo.Balance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BalanceRepository extends CrudRepository<Balance, Long> {
    Optional<Balance> findBalanceByAccountId(Long accountId);
}