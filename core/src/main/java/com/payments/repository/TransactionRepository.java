package com.payments.repository;

import com.payments.bo.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Long> {
    Page<Transaction> findTransactionsByAccount_IdOrderByActionedAtDesc(Long accountId, Pageable pageable);
}