package com.payments.repository;

import com.payments.bo.AccountHolder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountHolderRepository extends CrudRepository<AccountHolder, Long> {
    Optional<AccountHolder> findAccountHolderByIdCardNumber(String idCardNumber);
}