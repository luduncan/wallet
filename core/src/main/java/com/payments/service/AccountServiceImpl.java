package com.payments.service;

import com.payments.bo.Account;
import com.payments.bo.AccountHolder;
import com.payments.bo.Balance;
import com.payments.bo.Transaction;
import com.payments.enumerator.ErrorCode;
import com.payments.enumerator.TransactionType;
import com.payments.exception.InsufficientFundsException;
import com.payments.exception.InvalidAccountException;
import com.payments.repository.AccountHolderRepository;
import com.payments.repository.AccountRepository;
import com.payments.repository.BalanceRepository;
import com.payments.repository.TransactionRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    private final AccountHolderRepository accountHolderRepository;

    private final AccountRepository accountRepository;

    private final BalanceRepository balanceRepository;

    private final TransactionRepository transactionRepository;

    @Autowired
    public AccountServiceImpl(AccountHolderRepository accountHolderRepository, AccountRepository accountRepository, BalanceRepository balanceRepository, TransactionRepository transactionRepository) {
        this.accountHolderRepository = accountHolderRepository;
        this.accountRepository = accountRepository;
        this.balanceRepository = balanceRepository;
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Optional<Account> createAccount(@NonNull Account account) throws InvalidAccountException{
        log.info("Creating account with idCardNumber {}", account.getAccountHolder().getIdCardNumber());


        if(accountHolderRepository.findAccountHolderByIdCardNumber(account.getAccountHolder().getIdCardNumber()).isPresent()){
            throw new InvalidAccountException(ErrorCode.INVALID_ACCOUNT, String.format("Cannot create account with Id Card Number %s. User already exists", account.getAccountHolder().getIdCardNumber()));
        }

        Account savedAccount = accountRepository.save(account);
        account.getAccountHolder().setAccount(savedAccount);
        accountHolderRepository.save(account.getAccountHolder());

        Balance balance = account.getBalance();
        if (Objects.isNull(balance.getAsOf())) {
            balance.setAsOf(new Timestamp(DateTime.now().getMillis()));
        }

        balance.setAccount(account);
        balanceRepository.save(balance);
        transactionRepository.save(Transaction.builder()
                .amount(balance.getAmount())
                .account(account)
                .balance(balance.getAmount())
                .actionedBy(account.getAccountHolder())
                .transactionType(TransactionType.DEPOSIT.getValue())
                .actionedAt(new Timestamp(DateTime.now().getMillis()))
                .build());
        return Optional.ofNullable(savedAccount);
    }

    @Override
    public Optional<Account> deposit(String idCardNumber, BigDecimal amount) throws InvalidAccountException, InsufficientFundsException {
        log.info("Attempting to deposit {} in account associated with id card number{}", amount, idCardNumber);
        return Optional.of(processTransaction(idCardNumber, amount, TransactionType.DEPOSIT));
    }

    @Override
    public Optional<Account> withdraw(String idCardNumber, BigDecimal amount) throws InvalidAccountException, InsufficientFundsException {
        log.info("Attempting to withdraw {} from account with idCardNumber {}", amount, idCardNumber);
        return Optional.of(processTransaction(idCardNumber, amount, TransactionType.WITHDRAWAL));
    }

    @Override
    public Page<Transaction> getAccountTransactions(String idCardNumber, Integer pageNumber) throws InvalidAccountException {
        Account account = getAccountByIdCardNumber(idCardNumber);
        return transactionRepository.findTransactionsByAccount_IdOrderByActionedAtDesc(account.getId(), PageRequest.of(pageNumber, 10));
    }

    private Account getAccountByIdCardNumber(String idCardNumber) throws InvalidAccountException {
        Optional<AccountHolder> accountHolder = accountHolderRepository.findAccountHolderByIdCardNumber(idCardNumber);
        if (!accountHolder.isPresent()) {
            throw new InvalidAccountException(ErrorCode.INVALID_ACCOUNT, "Invalid account");
        }
        return accountHolder.get().getAccount();
    }

    private Account processTransaction(String idCardNumber, BigDecimal amount, TransactionType transactionType) throws InvalidAccountException, InsufficientFundsException {

        Account account = getAccountByIdCardNumber(idCardNumber);
        final Optional<Account> targetAccount = accountRepository.findById(account.getId());
        if(!targetAccount.isPresent()){
            throw new InvalidAccountException(ErrorCode.INVALID_ACCOUNT, "Invalid account");
        }

        if(transactionType == TransactionType.WITHDRAWAL && Money.of(account.getBalance().getAmount(), account.getCurrency().getCurrencyCode()).isLessThan(Money.of(amount, account.getCurrency().getCurrencyCode()))){
            throw new InsufficientFundsException(ErrorCode.INSUFFICIENT_FUNDS, "Insufficient Funds");
        }

        Balance accountBalance = targetAccount.get().getBalance();

        if(transactionType == TransactionType.DEPOSIT){
            final Money updatedAmount = Money.of(accountBalance.getAmount(), targetAccount.get().getCurrency().getCurrencyCode()).add(Money.of(amount, account.getCurrency().getCurrencyCode()));
            accountBalance.setAmount(new BigDecimal(updatedAmount.getNumber().doubleValueExact()));
        }

        if(transactionType == TransactionType.WITHDRAWAL){
            final Money updatedAmount = Money.of(accountBalance.getAmount(), targetAccount.get().getCurrency().getCurrencyCode()).subtract(Money.of(amount, account.getCurrency().getCurrencyCode()));
            accountBalance.setAmount(new BigDecimal(updatedAmount.getNumber().doubleValueExact()));
        }

        transactionRepository.save(Transaction.builder()
                .amount(amount)
                .account(account)
                .balance(accountBalance.getAmount())
                .actionedBy(account.getAccountHolder())
                .transactionType(transactionType.getValue())
                .actionedAt(new Timestamp(DateTime.now().getMillis()))
                .build());

        return account;
    }
}
