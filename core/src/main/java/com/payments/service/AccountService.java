package com.payments.service;

import com.payments.bo.Account;
import com.payments.api.*;
import com.payments.bo.Transaction;
import com.payments.exception.InsufficientFundsException;
import com.payments.exception.InvalidAccountException;
import org.javamoney.moneta.Money;
import org.springframework.data.domain.Page;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface AccountService {
    Optional<Account> createAccount(Account account) throws InvalidAccountException, InsufficientFundsException;
    Optional<Account> deposit(String idCardNumber, BigDecimal amount) throws InvalidAccountException, InsufficientFundsException;
    Optional<Account> withdraw(String idCardNumber, BigDecimal amount) throws InvalidAccountException, InsufficientFundsException;
    Page<Transaction> getAccountTransactions(String idCardNumber, Integer pageNumber) throws InvalidAccountException;
}