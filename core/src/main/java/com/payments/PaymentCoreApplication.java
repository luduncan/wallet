package com.payments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;


@SpringBootApplication
@PropertySource("classpath:application.properties")
public class PaymentCoreApplication {
    public static void main(String[] args) {
        SpringApplication.run(PaymentCoreApplication.class, args);
    }
}

