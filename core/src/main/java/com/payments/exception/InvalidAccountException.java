package com.payments.exception;

import com.payments.enumerator.ErrorCode;
import lombok.Getter;

public class InvalidAccountException extends Exception {

    @Getter
    private ErrorCode errorCode;

    public InvalidAccountException(ErrorCode errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
    }
}