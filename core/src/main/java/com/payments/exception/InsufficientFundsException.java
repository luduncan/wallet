package com.payments.exception;

import com.payments.enumerator.ErrorCode;
import lombok.Getter;

public class InsufficientFundsException extends Exception {

    @Getter
    private ErrorCode errorCode;

    public InsufficientFundsException(ErrorCode errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
    }
}