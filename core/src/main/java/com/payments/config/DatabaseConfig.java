package com.payments.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Data
@Component
@EnableJpaRepositories(basePackages = {
        "com.payments.bo",
        "com.payments.repository"
})
@EnableTransactionManagement
@ConfigurationProperties(prefix = "wallet.datasource")
public class DatabaseConfig {

    private String driverClassName;
    private String url;
    private String username;
    private String password;


    @Bean
    public DataSource dataSource() {
        return DataSourceBuilder.create()
                .driverClassName(driverClassName)
                .url(url)
                .username(username)
                .password(password)
                .build();
    }
}