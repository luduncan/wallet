package com.payments.bo;

import lombok.*;
import org.javamoney.moneta.Money;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;

    @NonNull
    @Column(name = "amount")
    private BigDecimal amount;

    @NonNull
    @Column(name = "balance")
    private BigDecimal balance;

    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL)
    private AccountHolder actionedBy;

    @NonNull
    @Column(name = "actionedAt")
    private Timestamp actionedAt;

    @NonNull
    @Column(name = "transactionType")
    private Integer transactionType;
}