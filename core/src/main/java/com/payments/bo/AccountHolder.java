package com.payments.bo;

import lombok.*;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Data
@Builder
@EqualsAndHashCode
@ToString(exclude = "account")
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "account_holder")
public class AccountHolder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NonNull
    @Column(name = "idCardNumber", unique = true)
    private String idCardNumber;

    @NonNull
    @Column(name = "name")
    private String name;

    @NonNull
    @Column(name = "surname")
    private String surname;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;
}