package com.payments.bo;

import lombok.*;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Currency;

@Data
@Builder
@ToString(exclude = "accountHolder")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private AccountHolder accountHolder;

    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Balance balance;

    @NonNull
    @Column(name = "currency")
    private Currency currency;
}