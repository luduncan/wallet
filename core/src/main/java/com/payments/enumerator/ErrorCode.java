package com.payments.enumerator;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
public enum ErrorCode {

    INVALID_ACCOUNT(-1),
    INSUFFICIENT_FUNDS(-2);

    @Getter
    private final Integer value;

    private static final Map<Integer, ErrorCode> lookup = Stream.of(ErrorCode.values())
            .collect(Collectors.toMap(ErrorCode::getValue, code -> code));

    public static ErrorCode get(Integer errorCode){
        return lookup.get(errorCode);
    }
}
