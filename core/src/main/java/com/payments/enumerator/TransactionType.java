package com.payments.enumerator;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
public enum TransactionType {
    DEPOSIT(1),
    WITHDRAWAL(2);

    @Getter
    private final Integer value;

    private static final Map<Integer, TransactionType> lookup = Stream.of(TransactionType.values())
            .collect(Collectors.toMap(TransactionType::getValue, type -> type));

    public static TransactionType get(Integer transactionType){
        return lookup.get(transactionType);
    }
}