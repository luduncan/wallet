package com.payments.repository;

import com.payments.bo.Account;
import com.payments.bo.AccountHolder;
import com.payments.bo.Balance;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Currency;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class AccountHolderRepositoryIntegrationTest extends BaseRepositoryIntegrationTest {

    @Autowired
    private AccountHolderRepository accountHolderRepository;

    @Autowired
    private AccountRepository accountRepository;

    private static AccountHolder HELLAS_BELLAS = AccountHolder.builder()
            .idCardNumber("90210M")
            .name("Hellas")
            .surname("Bellas")
            .build();

    private static Account HELLAS_ACCOUNT = Account.builder()
            .currency(Currency.getInstance("EUR"))
            .balance(Balance.builder()
                    .amount(new BigDecimal(100.00))
                    .asOf(new Timestamp(DateTime.now().getMillis()))
                    .build()
            )
            .accountHolder(HELLAS_BELLAS)
            .build();

    @Test
    public void shouldSaveAccounts(){
        accountHolderRepository.save(AccountHolder.builder()
                .name("Hellas")
                .surname("Bellas")
                .idCardNumber("90210M")
                .build());
        accountHolderRepository.save(AccountHolder.builder()
                .name("Luke")
                .surname("Duncan")
                .idCardNumber("123456M")
                .build());
        List<AccountHolder> accountHolders = (List<AccountHolder>) accountHolderRepository.findAll();
        assertEquals(2, accountHolders.size());
    }

    @Test
    public void shouldFindAccountHolderByIdCardNumber(){
        HELLAS_BELLAS.setAccount(HELLAS_ACCOUNT);
        accountHolderRepository.save(HELLAS_BELLAS);

        Optional<AccountHolder> accountHolder = accountHolderRepository.findAccountHolderByIdCardNumber(HELLAS_ACCOUNT.getAccountHolder().getIdCardNumber());
        assertTrue(accountHolder.isPresent());
        assertEquals("Hellas", accountHolder.get().getName());
        assertEquals(new BigDecimal(100.00), accountHolder.get().getAccount().getBalance().getAmount());
    }
}