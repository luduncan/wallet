package com.payments.repository;

import com.payments.bo.Account;
import com.payments.bo.AccountHolder;
import com.payments.bo.Balance;
import org.javamoney.moneta.Money;
import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Currency;
import java.util.Optional;

import static org.junit.Assert.*;

public class BusinessPaymentModelIntegrationTest extends BaseRepositoryIntegrationTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountHolderRepository accountHolderRepository;

    @Autowired
    private BalanceRepository balanceRepository;

    private static AccountHolder LUKE_DUNCAN = AccountHolder.builder()
            .idCardNumber("123456M")
            .name("Luke")
            .surname("Duncan")
            .build();

    private static AccountHolder HELLAS_BELLAS = AccountHolder.builder()
                .idCardNumber("90210M")
                .name("Hellas")
                .surname("Bellas")
                .build();


    @Test
    public void shouldCreateAccountsWithBalances(){
        final AccountHolder luke = accountHolderRepository.save(LUKE_DUNCAN);

        assertNotNull(luke);
        assertNotNull(luke.getId());
        assertEquals("Luke", luke.getName());
        assertEquals("Duncan", luke.getSurname());

        final AccountHolder hellas = accountHolderRepository.save(HELLAS_BELLAS);

        assertNotNull(hellas);
        assertNotNull(hellas.getId());
        assertEquals("Hellas", hellas.getName());
        assertEquals("Bellas", hellas.getSurname());

        final Account lukesAccount = accountRepository.save(Account.builder()
                .accountHolder(luke)
                .balance(Balance.builder()
                        .amount(new BigDecimal(50.40))
                        .asOf(new Timestamp(DateTime.now().getMillis()))
                        .build())
                .currency(Currency.getInstance("EUR"))
                .build());

        assertNotNull(lukesAccount);
        assertNotNull(lukesAccount.getId());
        assertNotNull(lukesAccount.getBalance());
        assertEquals(new BigDecimal(50.40), lukesAccount.getBalance().getAmount());

        final Account hellasAccount = accountRepository.save(Account.builder()
                .accountHolder(hellas)
                .balance(Balance.builder()
                        .amount(new BigDecimal(80.75))
                        .asOf(new Timestamp(DateTime.now().getMillis()))
                        .build())
                .currency(Currency.getInstance("EUR"))
                .build());

        assertNotNull(hellasAccount);
        assertNotNull(hellasAccount.getId());
        assertNotNull(hellasAccount.getBalance());
        assertEquals(new BigDecimal(80.75), hellasAccount.getBalance().getAmount());
    }

    @Test
    public void shouldGetDeductedAmountFromBalanceRepository(){
        final AccountHolder hellas = accountHolderRepository.save(HELLAS_BELLAS);
        final Account hellasAccount = accountRepository.save(Account.builder()
                .accountHolder(hellas)
                .currency(Currency.getInstance("EUR"))
                .build());

        Balance hellasBalance = balanceRepository.save(Balance.builder()
                .account(hellasAccount)
                .amount(new BigDecimal(80.75))
                .asOf(new Timestamp(DateTime.now().getMillis()))
                .build());

        hellasBalance.setAmount(
                Money.of(hellasBalance.getAmount(), hellasAccount.getCurrency().getCurrencyCode())
                        .subtract(Money.of(20.00, hellasAccount.getCurrency().getCurrencyCode()))
                        .getNumberStripped()
        );

        accountRepository.save(hellasAccount);

        final Optional<Balance> deductedBalance = balanceRepository.findBalanceByAccountId(hellasAccount.getId());

        assertTrue(deductedBalance.isPresent());
        assertEquals(new BigDecimal(60.75), deductedBalance.get().getAmount());
    }
}