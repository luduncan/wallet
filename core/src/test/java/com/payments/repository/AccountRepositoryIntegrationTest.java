package com.payments.repository;

import com.payments.bo.Account;
import com.payments.bo.AccountHolder;
import com.payments.bo.Balance;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Currency;
import java.util.List;


public class AccountRepositoryIntegrationTest extends BaseRepositoryIntegrationTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void shouldSaveAccounts(){
        accountRepository.save(Account.builder()
                .currency(Currency.getInstance("EUR"))
                .build());
        accountRepository.save(Account.builder()
                .currency(Currency.getInstance("GBP"))
                .build());
        List<Account> accounts = (List<Account>) accountRepository.findAll();
        Assert.assertEquals(2, accounts.size());
    }

}
