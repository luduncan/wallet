package com.payments.repository;

import com.payments.bo.Balance;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class BalanceRepositoryIntegrationTest extends BaseRepositoryIntegrationTest {

    @Autowired
    private BalanceRepository balanceRepository;

    @Test
    public void shouldSaveBalance(){
        balanceRepository.save(Balance.builder()
                .amount(new BigDecimal(10.02))
                .asOf(new Timestamp(DateTime.now().getMillis()))
                .build());
        List<Balance> balances = (List<Balance>) balanceRepository.findAll();
        Assert.assertEquals(1, balances.size());
    }
}
