package com.payments.repository;

import com.payments.bo.Transaction;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class TransactionRepositoryIntegrationTest extends BaseRepositoryIntegrationTest {

    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    public void shouldSaveTransaction(){
        transactionRepository.save(Transaction.builder()
                .amount(new BigDecimal(10.02))
                .actionedAt(new Timestamp(DateTime.now().getMillis()))
                .transactionType(0)
                .build());

        List<Transaction> transactions = (List<Transaction>) transactionRepository.findAll();
        Assert.assertEquals(1, transactions.size());
    }
}