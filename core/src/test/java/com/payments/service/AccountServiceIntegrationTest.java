package com.payments.service;

import com.payments.bo.Account;
import com.payments.bo.AccountHolder;
import com.payments.bo.Balance;
import com.payments.bo.Transaction;
import com.payments.exception.InsufficientFundsException;
import com.payments.exception.InvalidAccountException;
import com.payments.repository.*;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Currency;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class AccountServiceIntegrationTest extends BaseRepositoryIntegrationTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountHolderRepository accountHolderRepository;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    private AccountServiceImpl accountService;

    @Before
    public void setup(){
        accountService = new AccountServiceImpl(accountHolderRepository, accountRepository, balanceRepository, transactionRepository);
    }

    private static AccountHolder HELLAS_BELLAS = AccountHolder.builder()
            .idCardNumber("90210M")
            .name("Hellas")
            .surname("Bellas")
            .build();

    private static Account HELLAS_ACCOUNT = Account.builder()
            .currency(Currency.getInstance("EUR"))
            .balance(Balance.builder()
                    .amount(new BigDecimal(100.00))
                    .asOf(new Timestamp(DateTime.now().getMillis()))
                    .build()
            )
            .accountHolder(HELLAS_BELLAS)
            .build();

    @Test
    public void shouldCreateAccountHavingAssociatedAccountHolderAndBalance() throws InvalidAccountException, InsufficientFundsException {
        final Optional<Account> account = accountService.createAccount(HELLAS_ACCOUNT);

        assertTrue(account.isPresent());
        assertEquals("EUR", account.get().getCurrency().toString());

        assertNotNull(account.get().getAccountHolder());
        final AccountHolder accountHolder = account.get().getAccountHolder();

        assertEquals("Hellas", accountHolder.getName());
        assertEquals("Bellas", accountHolder.getSurname());
        assertEquals("90210M", accountHolder.getIdCardNumber());

        assertNotNull(account.get().getBalance());
        final Balance accountBalance = account.get().getBalance();
        assertEquals(new BigDecimal(100.00), accountBalance.getAmount());
    }

    @Test
    public void shouldDepositIntoAccount() throws InvalidAccountException, InsufficientFundsException {
        Optional<Account> hellasAccount = accountService.createAccount(Account.builder()
                .currency(Currency.getInstance("EUR"))
                .balance(Balance.builder()
                        .amount(new BigDecimal(100.00))
                        .asOf(new Timestamp(DateTime.now().getMillis()))
                        .build()
                )
                .accountHolder(AccountHolder.builder()
                        .idCardNumber("20000M")
                        .name("Hellas")
                        .surname("Bellas")
                        .build())
                .build());

        try {
            final BigDecimal amountToDeposit = new BigDecimal(100.00);
            accountService.deposit(hellasAccount.get().getAccountHolder().getIdCardNumber(), amountToDeposit);
            assertEquals(new BigDecimal(200.00), hellasAccount.get().getBalance().getAmount());
        } catch (InvalidAccountException | InsufficientFundsException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldWithdrawFromAccount() throws InvalidAccountException, InsufficientFundsException {
        Optional<Account> hellasAccount = accountService.createAccount(Account.builder()
                .currency(Currency.getInstance("EUR"))
                .balance(Balance.builder()
                        .amount(new BigDecimal(100.00))
                        .asOf(new Timestamp(DateTime.now().getMillis()))
                        .build()
                )
                .accountHolder(AccountHolder.builder()
                        .idCardNumber("30000M")
                        .name("Hellas")
                        .surname("Bellas")
                        .build())
                .build());
        try {
            final BigDecimal amountToWithdraw = new BigDecimal(50.00);
            accountService.withdraw(hellasAccount.get().getAccountHolder().getIdCardNumber(), amountToWithdraw);
            assertEquals(new BigDecimal(50.00), hellasAccount.get().getBalance().getAmount());
        } catch (InvalidAccountException | InsufficientFundsException e) {
            fail(e.getMessage());
        }
    }

    @Test(expected = InsufficientFundsException.class)
    public void shouldThrowInsufficientFundsWhenWithdrawingMoreThanTheMaxBalance() throws InvalidAccountException, InsufficientFundsException {
        Optional<Account> hellasAccount = accountService.createAccount(Account.builder()
                .currency(Currency.getInstance("EUR"))
                .balance(Balance.builder()
                        .amount(new BigDecimal(100.00))
                        .asOf(new Timestamp(DateTime.now().getMillis()))
                        .build()
                )
                .accountHolder(AccountHolder.builder()
                        .idCardNumber("40000M")
                        .name("Hellas")
                        .surname("Bellas")
                        .build())
                .build());
        final BigDecimal amountToWithdraw = new BigDecimal(200.00);
        accountService.withdraw(hellasAccount.get().getAccountHolder().getIdCardNumber(), amountToWithdraw);
    }

    @Test
    public void shouldGetListOfTransactions() throws InvalidAccountException, InsufficientFundsException {
        Account account = Account.builder()
                .currency(Currency.getInstance("EUR"))
                .balance(Balance.builder()
                        .amount(new BigDecimal(100.00))
                        .asOf(new Timestamp(DateTime.now().getMillis()))
                        .build()
                )
                .accountHolder(AccountHolder.builder()
                        .idCardNumber("123695M")
                        .name("Hellas")
                        .surname("Bellas")
                        .build())
                .build();

        Optional<Account> hellasAccount = accountService.createAccount(account);
        try {
            accountService.deposit(hellasAccount.get().getAccountHolder().getIdCardNumber(), new BigDecimal(1000.00));
            accountService.withdraw(hellasAccount.get().getAccountHolder().getIdCardNumber(), new BigDecimal(50.00));
            accountService.deposit(hellasAccount.get().getAccountHolder().getIdCardNumber(), new BigDecimal(65.00));
            accountService.withdraw(hellasAccount.get().getAccountHolder().getIdCardNumber(), new BigDecimal(52.00));
            accountService.withdraw(hellasAccount.get().getAccountHolder().getIdCardNumber(), new BigDecimal(60.00));

            final Page<Transaction> accountTransactions = accountService.getAccountTransactions(hellasAccount.get().getAccountHolder().getIdCardNumber(), 1);
            final List<Transaction> transactions = accountTransactions.getContent();
            assertNotNull(transactions);
            assertEquals(5, transactions.size());
        } catch (InvalidAccountException | InsufficientFundsException e) {
            fail(e.getMessage());
        }
    }
}