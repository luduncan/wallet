package com.payments.http.response;

import com.payments.api.ApiAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateAccountHttpResponse {
    private ApiAccount apiAccount;
    private String errorMessage;
}