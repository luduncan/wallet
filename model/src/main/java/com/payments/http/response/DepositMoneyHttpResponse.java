package com.payments.http.response;

import com.payments.api.ApiAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DepositMoneyHttpResponse implements Serializable {
    private static final long serialVersionUID = 8069358006637213570L;
    private ApiAccount account;
    private String errorMessage;
}