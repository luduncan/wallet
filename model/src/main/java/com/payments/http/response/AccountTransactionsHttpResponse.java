package com.payments.http.response;

import com.payments.api.ApiTransaction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountTransactionsHttpResponse {

    private Integer pageNumber;
    private List<ApiTransaction> transactions;
    private String errorMessage;
}