package com.payments.http.request;

import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WithdrawMoneyHttpRequest implements Serializable {
    private static final long serialVersionUID = -1671038537053169015L;

    @NotBlank
    private String accountHolderIdCardNumber;

    @NonNull
    private BigDecimal amount;
}
