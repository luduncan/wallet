package com.payments.http.request;

import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateAccountHttpRequest implements Serializable {
    private static final long serialVersionUID = 8333597465885583647L;

    @NotBlank
    private String name;

    @NotBlank
    private String surname;

    @NotBlank
    private String idCardNumber;

    @NonNull
    private BigDecimal accountBalance;

    @NotBlank
    private String accountCurrency;
}