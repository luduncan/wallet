package com.payments.http.request;


import lombok.*;
import org.hibernate.validator.constraints.NotBlank;
import org.javamoney.moneta.Money;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DepositMoneyHttpRequest implements Serializable {
    private static final long serialVersionUID = -8097460678117187234L;

    @NotBlank
    private String accountHolderIdCardNumber;

    @NonNull
    private BigDecimal amount;
}