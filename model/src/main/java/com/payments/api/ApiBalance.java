package com.payments.api;

import lombok.*;
import org.javamoney.moneta.Money;
import org.joda.time.DateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiBalance {

    @NonNull
    private String amount;

    private String asOf;
}