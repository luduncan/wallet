package com.payments.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javamoney.moneta.Money;
import org.joda.time.DateTime;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiTransaction {
    private BigDecimal amount;
    private BigDecimal balance;
    private String actionedAt;
    private ApiTransactionType transactionType;
}