package com.payments.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiAccountHolder {

    @NotBlank
    private String name;

    @NotBlank
    private String surname;

    @NotBlank
    private String idCardNumber;
}