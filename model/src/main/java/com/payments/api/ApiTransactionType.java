package com.payments.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
public enum ApiTransactionType {
    UNKNOWN(0),
    DEPOSIT(1),
    WITHDRAWAL(2);

    @Getter
    private final Integer type;

    private static final Map<Integer, ApiTransactionType> lookup = Stream.of(ApiTransactionType.values())
            .collect(Collectors.toMap(ApiTransactionType::getType, type -> type));

    public static ApiTransactionType get(Integer type){
        return lookup.get(type);
    }
}