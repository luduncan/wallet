package com.payments.api;

import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Currency;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiAccount {

    @NonNull
    private ApiAccountHolder accountHolder;

    @NonNull
    private ApiBalance accountBalance;

    @NonNull
    private Currency currency;
}