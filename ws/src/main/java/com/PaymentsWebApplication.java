package com;

import com.payments.PaymentCoreApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentsWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(PaymentCoreApplication.class, args);
    }
}