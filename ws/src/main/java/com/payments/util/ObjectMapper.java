package com.payments.util;

import com.payments.api.*;
import com.payments.bo.Account;
import com.payments.bo.AccountHolder;
import com.payments.bo.Balance;
import com.payments.bo.Transaction;
import com.payments.http.request.CreateAccountHttpRequest;
import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.util.Currency;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class ObjectMapper {

    public Account mapCreateAccountHttpRequestToAccountBo(CreateAccountHttpRequest request){
        return Account.builder()
                .currency(Currency.getInstance(request.getAccountCurrency()))
                .accountHolder(AccountHolder.builder()
                        .idCardNumber(request.getIdCardNumber())
                        .name(request.getName())
                        .surname(request.getSurname())
                        .build())
                .balance(Balance.builder()
                        .amount(request.getAccountBalance())
                        .asOf(new Timestamp(DateTime.now().getMillis()))
                        .build())
                .build();
    }

    public ApiAccount mapAccountBoToApiAccount(Account account){
        return Objects.isNull(account) ? ApiAccount.builder().build() :
                ApiAccount.builder()
                        .currency(Optional.ofNullable(account.getCurrency()).orElse(null))
                        .accountHolder(
                                Objects.isNull(account.getAccountHolder()) ? ApiAccountHolder.builder().build() :
                                ApiAccountHolder.builder()
                                    .name(Optional.ofNullable(account.getAccountHolder().getName()).orElse(""))
                                    .surname(Optional.ofNullable(account.getAccountHolder().getSurname()).orElse(""))
                                    .idCardNumber(Optional.ofNullable(account.getAccountHolder().getIdCardNumber()).orElse(""))
                                .build()
                        )
                        .accountBalance(
                                Objects.isNull(account.getBalance()) ? ApiBalance.builder().build() :
                                ApiBalance.builder()
                                    .amount(account.getBalance().getAmount().toString())
                                    .asOf(account.getBalance().getAsOf().toString())
                                .build()
                        )
                    .build();
    }

    public List<ApiTransaction> mapTransactionBoListToApiTransactionList(List<Transaction> transactions) {
        return transactions.stream()
                .filter(Objects::nonNull)
                .map(transaction -> ApiTransaction.builder()
                            .actionedAt(Optional.ofNullable(transaction.getActionedAt().toString()).orElse(""))
                            .amount(Optional.ofNullable(transaction.getAmount()).orElse(null))
                            .balance(Optional.ofNullable(transaction.getBalance()).orElse(null))
                            .transactionType(ApiTransactionType.get(Optional.ofNullable(transaction.getTransactionType()).orElse(0)))
                        .build())
                .collect(Collectors.toList());
    }
}