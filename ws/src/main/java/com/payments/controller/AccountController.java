package com.payments.controller;

import com.payments.bo.Account;
import com.payments.exception.InsufficientFundsException;
import com.payments.exception.InvalidAccountException;
import com.payments.http.request.CreateAccountHttpRequest;
import com.payments.http.request.DepositMoneyHttpRequest;
import com.payments.http.request.WithdrawMoneyHttpRequest;
import com.payments.http.response.AccountTransactionsHttpResponse;
import com.payments.http.response.CreateAccountHttpResponse;
import com.payments.http.response.DepositMoneyHttpResponse;
import com.payments.http.response.WithdrawMoneyHttpResponse;
import com.payments.service.AccountService;
import com.payments.util.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotBlank;
import java.util.Optional;
import java.util.Set;

@Slf4j
@RestController
public class AccountController {

    private final AccountService accountService;

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/account/create")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ApiAccount created successful"),
            @ApiResponse(code = 400, message = "The received request is not syntactically valid. Correct the request and try again.")
    })
    @ApiOperation(value = "Create ApiAccount", notes = "Creates an account and an account holder having a balance")
    public CreateAccountHttpResponse createAccount(@RequestBody CreateAccountHttpRequest request){
        try{
            final Set<ConstraintViolation<CreateAccountHttpRequest>> constraintViolations = validator.validate(request);
            if (!CollectionUtils.isEmpty(constraintViolations)) {
                log.error("Request received to create account has validation constraint violations: [{}]", constraintViolations);
                return CreateAccountHttpResponse.builder().errorMessage("Request received to create account has validation constraint violations:").build();
            }

            Optional<Account> account = accountService.createAccount(objectMapper.mapCreateAccountHttpRequestToAccountBo(request));
            return account.map(acc -> CreateAccountHttpResponse.builder()
                    .apiAccount(objectMapper.mapAccountBoToApiAccount(acc))
                    .build()).orElse(CreateAccountHttpResponse.builder().errorMessage("Failed to create account!").build());
        } catch (InvalidAccountException | InsufficientFundsException e) {
            log.error("Invalid account selection with idCardNumber {}", request.getIdCardNumber());
            return CreateAccountHttpResponse.builder().errorMessage(e.getMessage()).build();
        }
    }

    @PostMapping("/account/deposit")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Deposit successful"),
            @ApiResponse(code = 400, message = "The received request is not syntactically valid. Correct the request and try again.")
    })
    @ApiOperation(value = "Deposit", notes = "Add funds to a target account")
    public DepositMoneyHttpResponse deposit(@RequestBody DepositMoneyHttpRequest request){
        try {
            final Set<ConstraintViolation<DepositMoneyHttpRequest>> constraintViolations = validator.validate(request);
            if (!CollectionUtils.isEmpty(constraintViolations)) {
                log.error("Request received to deposit funds has validation constraint violations: [{}]", constraintViolations);
                return DepositMoneyHttpResponse.builder().errorMessage("Request received to deposit money has validation constraint violations:").build();
            }
            Optional<Account> account = accountService.deposit(request.getAccountHolderIdCardNumber(), request.getAmount());
            return account.map(acc -> DepositMoneyHttpResponse.builder()
                    .account(objectMapper.mapAccountBoToApiAccount(acc))
                    .build()).orElse(DepositMoneyHttpResponse.builder().errorMessage("Failed to deposit into target account!").build());

        } catch (InvalidAccountException | InsufficientFundsException e) {
            log.error("Invalid account selection with idCardNumber {}", request.getAccountHolderIdCardNumber());
            return DepositMoneyHttpResponse.builder().errorMessage(e.getMessage()).build();
        }
    }

    @PostMapping("/account/withdraw")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Withdrawal successful"),
            @ApiResponse(code = 400, message = "The received request is not syntactically valid. Correct the request and try again.")
    })
    @ApiOperation(value = "Withdraw", notes = "Withdraw funds from a target account")
    public WithdrawMoneyHttpResponse deposit(@RequestBody WithdrawMoneyHttpRequest request){
        try {
            final Set<ConstraintViolation<WithdrawMoneyHttpRequest>> constraintViolations = validator.validate(request);
            if (!CollectionUtils.isEmpty(constraintViolations)) {
                log.error("Request received to withdraw funds has validation constraint violations: [{}]", constraintViolations);
                return WithdrawMoneyHttpResponse.builder().errorMessage("Request received to withdraw funds has validation constraint violations:").build();
            }
            Optional<Account> account = accountService.withdraw(request.getAccountHolderIdCardNumber(), request.getAmount());
            return account.map(acc -> WithdrawMoneyHttpResponse.builder()
                    .account(objectMapper.mapAccountBoToApiAccount(acc))
                    .build()).orElse(WithdrawMoneyHttpResponse.builder().errorMessage("Failed to withdraw from target account!").build());

        } catch (InvalidAccountException e) {
            log.error("Invalid account selection with idCardNumber {}", request.getAccountHolderIdCardNumber());
            return WithdrawMoneyHttpResponse.builder().errorMessage(e.getMessage()).build();
        } catch (InsufficientFundsException e) {
            log.error("Insufficient funds for account holder with id card number {}", request.getAccountHolderIdCardNumber());
            return WithdrawMoneyHttpResponse.builder().errorMessage(e.getMessage()).build();
        }
    }

    @GetMapping("/account/transactions/{idCardNumber}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "List transactions successful"),
            @ApiResponse(code = 400, message = "The received request is not syntactically valid. Correct the request and try again.")
    })
    @ApiOperation(value = "List Transactions", notes = "List transactions of a target account")
    public AccountTransactionsHttpResponse getTransactions(@PathVariable String idCardNumber, @RequestParam Integer page){
        try {
            final Set<ConstraintViolation<AccountTransactionParams>> constraintViolations = validator.validate(AccountTransactionParams.builder()
                        .idCardNumber(idCardNumber)
                        .page(page)
                    .build());
            if (!CollectionUtils.isEmpty(constraintViolations)) {
                log.error("Request received to list transactions has validation constraint violations: [{}]", constraintViolations);
                return AccountTransactionsHttpResponse.builder().errorMessage("Request received to list transactions has validation constraint violations:").build();
            }

            return AccountTransactionsHttpResponse.builder()
                    .transactions(objectMapper.mapTransactionBoListToApiTransactionList(accountService.getAccountTransactions(idCardNumber, page).getContent()))
                    .pageNumber(page)
                    .errorMessage("")
                    .build();

        } catch (InvalidAccountException e) {
            log.error("Invalid account selection with idCardNumber {}", idCardNumber);
            return AccountTransactionsHttpResponse.builder().errorMessage(e.getMessage()).build();
        }
    }

    @Data
    @Builder
    public static class AccountTransactionParams{
        @NotBlank
        private String idCardNumber;

        @NonNull
        private Integer page;
    }
}