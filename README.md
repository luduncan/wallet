# wallet

Pre requisites

- Java 8 or higher
- Maven 2 or higher
- Mysql 5.5 or higher

Create an empty MySql schema having the name 'wallet'. 

You can find database properties (username and password) in the application.properties file located in the core module's resource folder.

Build the aggregator module using Maven in order to generate packages. You should deploy the jar 'wallet-ws-1.0.0' found in the ws module. Use 'java -jar wallet-ws-1.0.0.jar' to run the application. 
(Please note that you should have http traffic enabled when deploying to a remote server). The application runs on port 8080 by default. You can pass the property -Dserver.port=<portNumber> in your run command should you want an alternate port

Below are some curl tests used whilst developing.

- Create an account:

curl -X POST \
  http://localhost:8080/account/create \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 129' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  -H 'Postman-Token: 1960afa5-eb20-4858-96e2-78fe3226f02c,69f5f05e-08a4-4a73-bf2a-2e0c8698efc9' \
  -H 'User-Agent: PostmanRuntime/7.15.2' \
  -H 'cache-control: no-cache' \
  -d '{
	"name" : "Luke",
	"surname" : "Duncan",
	"idCardNumber" : "354321M",
	"accountBalance" : 1000.00,
	"accountCurrency" : "EUR"
}'

- Deposit to account:

curl -X POST \
  http://localhost:8080/account/deposit \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 64' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  -H 'Postman-Token: 159c0c6d-fd46-4e53-82ab-7b61927c0f8f,65f3b343-fb4c-400f-ac81-0fca0f0daf1f' \
  -H 'User-Agent: PostmanRuntime/7.15.2' \
  -H 'cache-control: no-cache' \
  -d '{
	"accountHolderIdCardNumber" : "354321M",
	"amount" : 900.00
}'

- Withdraw from account:

curl -X POST \
  http://localhost:8080/account/withdraw \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 0cd6a2ae-f115-4145-a025-fbd3b705f2af' \
  -H 'cache-control: no-cache' \
  -d '{
	"accountHolderIdCardNumber" : "354321M",
	"amount" : 10.00
}'

- List transactions:

curl -X GET \
  'http://localhost:8080/account/transactions/354321M?page=0' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 5ab015c7-1d16-4530-92cd-62aa47b28700' \
  -H 'cache-control: no-cache'